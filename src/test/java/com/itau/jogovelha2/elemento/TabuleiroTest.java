package com.itau.jogovelha2.elemento;

import static org.junit.Assert.assertEquals;


import org.junit.Test;

public class TabuleiroTest {

	@Test
	public void deveRetornarTabuleiroVazio() {
		Tabuleiro tabuleiro = new Tabuleiro();
		
		//assertTrue(tabuleiro.toString().equals("[][][]\n[][][]\n[][][]\n"));
		//assertTrue(tabuleiro.toString().equals(""));
		tabuleiro.inicializar();
		assertEquals(tabuleiro.toString(), "[ ][ ][ ]\n[ ][ ][ ]\n[ ][ ][ ]\n");
	}
	
	@Test
	public void deveDefinirCasa() {
		Tabuleiro tabuleiro = new Tabuleiro();
		tabuleiro.inicializar();
		tabuleiro.setCasa(0, 0, Valor.X);
		tabuleiro.setCasa(1, 1, Valor.O);
		
	}
}

